import React, { useMemo, useState} from 'react';
import Spinner from "../../componenets/Spinner/Spinner";



const withSpinner = (WrappedComponent, axios) => {

    return function WithSpinnerHOC(props) {
        const [loading, setLoading] = useState(false);

        useMemo(() => {
            axios.interceptors.request.use(req => {
                setLoading(true);
                return req;
            })
            axios.interceptors.response.use(res => {
                setLoading(false);
                return res;
            }, err => {
                setLoading(false);
                throw err;
            })

        }, []);

        let showSpinner = null;

        if (loading) {
            showSpinner = <Spinner/>
        }
        return (
            <>
                {showSpinner}
                <WrappedComponent {...props} />

            </>
        )

    }
};

export default withSpinner;