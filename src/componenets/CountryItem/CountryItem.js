import React from 'react';
import './CountryItem.css';
const CountryItem = props => {
    return (
        <option
            className='item'
        >
            {props.name}
        </option>
    );
};

export default CountryItem;