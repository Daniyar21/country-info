import './App.css';
import {useEffect, useState} from "react";
import CountryItem from "./componenets/CountryItem/CountryItem";
import axios from "axios";
import withSpinner from "./hoc/withSpinner/withSpinner";

const App = () => {

    const [listCountries,setListCountries] = useState([]);
    const [countryBorders, setCountryBorders]=useState([]);
    const [finalBorder,setFinalBorder] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code');

          const allCountries =  response.data.map(country => {
             return { countryName: country.name };
            });

          setListCountries(allCountries);
        }
        fetchData().catch(e => console.error(e));
    }, []);


    const onSelectClick = name => {
        const fetchData = async () => {
            const url = 'https://restcountries.eu/rest/v2/name/'+ name.toLowerCase();
            const response = await axios.get(url);
            setCountryBorders(response.data[0].borders);
        }
        fetchData().catch(e => console.error(e));

    }

    useEffect(() => {
        const fetchData = async () => {
            const responses = [];
            for (let i = 0; i < countryBorders.length; i++) {
                const response = await axios.get('https://restcountries.eu/rest/v2/alpha/'+countryBorders[i]);
                responses.push(response);
            }
            const finalResponse = await Promise.all(responses);

            const names = finalResponse.map(b=>{
                return b.data;
            })

            const borderNames = names.map(b=>{
                return b.name
            })

            setFinalBorder(borderNames);

        }
        fetchData().catch(e => console.error(e));
    }, [countryBorders]);


    const listOfAllCountries = listCountries.map((c,i)=>(
        <CountryItem
            key = {i}
            name = {c.countryName}
        />
    ))

  return (
      <div className="App">

          <div className='countries'>
              <select name="listCountry" id="list-country" onChange={e=>onSelectClick(e.target.value)}>
                  {listOfAllCountries}
              </select>
          </div>
          <div className='borders'>
              {finalBorder.map(country =>(
                  <p>{country}</p>
              ))}
          </div>

      </div>
  );
};

export default withSpinner(App,axios);
